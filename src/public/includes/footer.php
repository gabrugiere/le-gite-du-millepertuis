<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="footer__title">Coordonées</div>
                <address class="address">
                    <div class="address__title">Adresse</div>
                    <p>Le Gîte du Millepertuis<br/>Aline et Bruno BRUGIÈRE<br/>Reboisson<br/>63680 Chastreix</p>
                    <div class="address__title">Téléphone</div>
                    <p><a class="js-tel-link" href="#">04 XX XX XX XX</a></p>
                    <div class="address__title">E-Mail</div>
                    <p><a class="js-mail-link" href="#">xxxxx@xxx.xx</a></p>
                </address>
            </div>

            <div class="col-md-7 col-lg-5 offset-lg-1">
                <div class="financing">
                    <div class="row">
                        <div class="col-12 mb-3 mb-md-4 d-flex justify-content-center align-items-center">
                            <a href="https://www.accueil-paysan.com/fr/"><img class="financing__img" src="build/images/logo/accueil-paysan.be363a93.jpg" loading="lazy" alt="Logo accueil paysan" title="Logo accueil paysan"/></a>
                        </div>
                        <div class="col-12 mb-5">
                            <a href="subventions.php">Nos subventions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="credits">
        © 2020 - <?= date('Y') ?> | Créé par <a class="credits__link" href="https://www.gabrugiere.net">Gaëthan BRUGIERE</a>.
    </div>
</footer>
